#!/bin/bash
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=56
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
module purge                      # 標準で有効なmoduleをpurgeで全てunload
module load gcc/4.8.5             # Gcc-4.8.5のmoduleをload
module load openfoam/v1812        # OpenFOAM-v1812のmoduleをload
source $WM_PROJECT_DIR/etc/bashrc # OpenFOAMの環境設定
export I_MPI_DEBUG=5              # Intel MPIのDEBUG情報レベル
# foamRunTutorialsコマンドでチュートリアルを実行
foamRunTutorials >& log.$PJM_JOBNAME.${PJM_SUBJOBID:-$PJM_JOBID}
