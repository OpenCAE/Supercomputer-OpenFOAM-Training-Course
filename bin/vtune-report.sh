#!/bin/bash

module load vtune

for dir in vtune.*
do
    [ -d $dir ] || continue
    [ -f $dir/.norun ] || continue
    output=$dir.txt
    [ -f $output ] || amplxe-cl -R hotspots -r $dir > $output
done
