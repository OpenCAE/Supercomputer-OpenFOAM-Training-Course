#!/bin/sh
#PJM -g gt00
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=56
#PJM -L elapse=0:15:00
#PJM -S
module purge                                   # ここからOpenFOAMの環境設定
module load gcc
module load openfoam/v1812
source $WM_PROJECT_DIR/etc/bashrc              # ここまでOpenFOAMの環境設定 
reconstructPar -fields '(alpha.water p U)' >& log.reconstructPar # 再構築
# -fields: 再構築する場のリストを指定．指定無き場合には全場を再構築する．
