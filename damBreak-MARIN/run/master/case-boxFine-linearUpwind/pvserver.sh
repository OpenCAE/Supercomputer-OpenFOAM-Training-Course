#!/bin/bash
#PJM -g gt00
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=56
#PJM -L elapse=0:15:00
#PJM -S
module purge                 # ここからParaView-5.6.1の環境設定
module load intel/2019.4.243
module load llvm/7.1.0
module load paraview/5.6.1   # ここまでParaView-5.6.1の環境設定     
# pvserverを起動
pvserver --force-offscreen-rendering --disable-xdisplay-test >& log.pvserver
