set terminal x11 font "helvetica,17" linewidth 1.5 persist
set xlabel "time [s]"
set ylabel "pressure [Pa]"
set xrange [0.3:2]
set style data line
plot '../../SPHERIC_Test2/case.txt' u 1:2 t "Exp." with l lw 2\
, '../damBreakMARIN/postProcessing/probes/0/p' u 1:2 t "damBreakMARIN"\
, '../case-WALE/postProcessing/probes/0/p' u 1:2 t "case-WALE"\
, '../case-kOmegaSST/postProcessing/probes/0/p' u 1:2 t "case-kOmegaSST"\
, '../case-linearUpwind/postProcessing/probes/0/p' u 1:2 t "case-linearUpwind"\
, '../case-LUST/postProcessing/probes/0/p' u 1:2 t "case-LUST"\
, '../case-boxFine/postProcessing/probes/0/p' u 1:2 t "case-boxFine"\
, '../case-boxFine-linearUpwind/postProcessing/probes/0/p' u 1:2 t "case-boxFine-linearUpwind"\
, '../case-boxFine-LUST/postProcessing/probes/0/p' u 1:2 lc 0 t "case-boxFine-LUST"\
#, '../case-linear/postProcessing/probes/0/p' u 1:2 t "case-linear"\
#, '../case-boxFine-LUST/postProcessing/probes/0/p' u 1:2 lc 0 t "case-boxFine-linear"\
#, '../case-box2H/postProcessing/probes/0/p' u 1:2 t "case-box2H"\
#, '../case-OF6/postProcessing/probes/0/p' u 1:2 t "case-OF6"
