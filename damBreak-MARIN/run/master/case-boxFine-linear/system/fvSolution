/*--------------------------------*- C++ -*----------------------------------*\
| =========                 |                                                 |
| \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox           |
|  \\    /   O peration     | Version:  plus                                  |
|   \\  /    A nd           | Web:      www.OpenFOAM.com                      |
|    \\/     M anipulation  |                                                 |
\*---------------------------------------------------------------------------*/
FoamFile
{
    version     2.0;
    format      ascii;
    class       dictionary;
    location    "system";
    object      fvSolution;
}
// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //

solvers //線形ソルバ設定
{
    "alpha.water.*" //液相率のVOF解法設定
    {
        nAlphaCorr      1; //解法反復回数
        nAlphaSubCycles 3; //サブサイクル回数
        cAlpha          1; // 相界面圧縮に関する係数(0-2，デフォルト1)
    }

    p_rgh //静水圧を除いた圧力のPoisson方程式に対する線形ソルバ
    {
        solver          GAMG;  //線形ソルバ: 代数マルチグリッド
        tolerance       1e-08; //収束判定残差
        relTol          0.01;  //収束判定残差(初期残差との比)
        smoother        DIC;   //スムーザ: ガウスザイデル
        cacheAgglomeration no; //格子の粗視化情報のキャッシュ
    }

    p_rghFinal //静水圧を除いた圧力のPoisson方程式に対する線形ソルバ(反復最終時)
    {
        $p_rgh; //p_rghの設定を再利用
        relTol          0; //収束判定として初期残差との比は用いず，toleranceのみ考慮
    }

    "pcorr.*" //格子移動時の流束補正用項に対する線形ソルバ
    {
        $p_rghFinal; //p_rghFinalの設定を再利用
        tolerance       0.0001;
    }
//! 下の行を変更(U → "U.*")
    "U.*" // Uの輸送方程式に対する線形ソルバ
    {
        solver          smoothSolver; //平滑型ソルバ
        smoother        GaussSeidel; //スムーザ: ガウスザイデル
        tolerance       1e-06;
        relTol          0;
        nSweeps         1; //スイープ数
    }

    "(k|omega|B|nuTilda).*" //k,omega,B,nuTilda等の輸送方程式に対する線形ソルバ
    {
        solver          smoothSolver; //平滑型ソルバ
        smoother        symGaussSeidel; //スムーザ: ガウスザイデル(両方向スイープ)
        tolerance       1e-08;
        relTol          0;
    }
}

PIMPLE //PIMPLE法の設定
{
//! 下の行を変更(no → yes)
    momentumPredictor yes; //運動量輸送方程式を解いて運動量予測値を算出
    nCorrectors     3;    //PIMPLE法の内反復回数
    nNonOrthogonalCorrectors 0; //非直交補正反復回数
//! ここからコメントアウト
/*
    pRefPoint      (0.51 0.51 0.51);
    pRefValue      0;
*/
//! ここまでコメントアウト
}
