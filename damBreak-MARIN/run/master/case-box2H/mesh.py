# state file generated using paraview version 5.6.2

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.6.2
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [2152, 1276]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [1.61000001430511, 0.0, 0.499999996873555]
renderView1.StereoType = 0
renderView1.CameraPosition = [-0.271758686641025, -2.7724400175163, 2.76899733692044]
renderView1.CameraFocalPoint = [2.65338709222251, 1.79815250973452, -1.31912008615726]
renderView1.CameraViewUp = [0.22027131639954403, 0.5681965861895442, 0.7928639143094861]
renderView1.CameraParallelScale = 1.75843681978879
renderView1.Background = [0.32, 0.34, 0.43]
renderView1.OSPRayMaterialLibrary = materialLibrary1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.XTitleFontFile = ''
renderView1.AxesGrid.YTitleFontFile = ''
renderView1.AxesGrid.ZTitleFontFile = ''
renderView1.AxesGrid.XLabelFontFile = ''
renderView1.AxesGrid.YLabelFontFile = ''
renderView1.AxesGrid.ZLabelFontFile = ''

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(FileName='pv.foam')
pvfoam.MeshRegions = ['box_maxX', 'box_maxY', 'box_maxZ', 'box_minX', 'box_minY', 'internalMesh', 'region_maxX', 'region_maxY', 'region_maxZ', 'region_minX', 'region_minY', 'region_minZ']
pvfoam.Readzones = 1

# create a new 'Extract Block'
extractBlock2 = ExtractBlock(Input=pvfoam)
extractBlock2.BlockIndices = [17]

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(Input=pvfoam)
extractBlock1.BlockIndices = [12, 13, 9, 10, 11, 4, 6, 7]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from extractBlock1
extractBlock1Display = Show(extractBlock1, renderView1)

# get color transfer function/color map for 'vtkBlockColors'
vtkBlockColorsLUT = GetColorTransferFunction('vtkBlockColors')
vtkBlockColorsLUT.InterpretValuesAsCategories = 1
vtkBlockColorsLUT.AnnotationsInitialized = 1
vtkBlockColorsLUT.Annotations = ['0', '0', '1', '1', '2', '2', '3', '3', '4', '4', '5', '5', '6', '6', '7', '7', '8', '8', '9', '9', '10', '10', '11', '11']
vtkBlockColorsLUT.ActiveAnnotatedValues = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '0', '1', '2', '3', '4', '5', '6', '7']
vtkBlockColorsLUT.IndexedColors = [1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.6299992370489051, 0.6299992370489051, 1.0, 0.6699931334401464, 0.5000076295109483, 0.3300068665598535, 1.0, 0.5000076295109483, 0.7499961852445258, 0.5300068665598535, 0.3499961852445258, 0.7000076295109483, 1.0, 0.7499961852445258, 0.5000076295109483]
vtkBlockColorsLUT.IndexedOpacities = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]

# trace defaults for the display properties.
extractBlock1Display.Representation = 'Surface With Edges'
extractBlock1Display.ColorArrayName = ['FIELD', 'vtkBlockColors']
extractBlock1Display.LookupTable = vtkBlockColorsLUT
extractBlock1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock1Display.SelectOrientationVectors = 'None'
extractBlock1Display.ScaleFactor = -2.0000000000000002e+298
extractBlock1Display.SelectScaleArray = 'None'
extractBlock1Display.GlyphType = 'Arrow'
extractBlock1Display.GlyphTableIndexArray = 'None'
extractBlock1Display.GaussianRadius = -1e+297
extractBlock1Display.SetScaleArray = [None, '']
extractBlock1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock1Display.OpacityArray = [None, '']
extractBlock1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock1Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock1Display.SelectionCellLabelFontFile = ''
extractBlock1Display.SelectionPointLabelFontFile = ''
extractBlock1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
extractBlock1Display.DataAxesGrid.XTitleFontFile = ''
extractBlock1Display.DataAxesGrid.YTitleFontFile = ''
extractBlock1Display.DataAxesGrid.ZTitleFontFile = ''
extractBlock1Display.DataAxesGrid.XLabelFontFile = ''
extractBlock1Display.DataAxesGrid.YLabelFontFile = ''
extractBlock1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
extractBlock1Display.PolarAxes.PolarAxisTitleFontFile = ''
extractBlock1Display.PolarAxes.PolarAxisLabelFontFile = ''
extractBlock1Display.PolarAxes.LastRadialAxisTextFontFile = ''
extractBlock1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# show data from extractBlock2
extractBlock2Display = Show(extractBlock2, renderView1)

# trace defaults for the display properties.
extractBlock2Display.Representation = 'Wireframe'
extractBlock2Display.ColorArrayName = [None, '']
extractBlock2Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock2Display.SelectOrientationVectors = 'None'
extractBlock2Display.ScaleFactor = 0.322000002861023
extractBlock2Display.SelectScaleArray = 'None'
extractBlock2Display.GlyphType = 'Arrow'
extractBlock2Display.GlyphTableIndexArray = 'None'
extractBlock2Display.GaussianRadius = 0.016100000143051147
extractBlock2Display.SetScaleArray = [None, '']
extractBlock2Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock2Display.OpacityArray = [None, '']
extractBlock2Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock2Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock2Display.SelectionCellLabelFontFile = ''
extractBlock2Display.SelectionPointLabelFontFile = ''
extractBlock2Display.PolarAxes = 'PolarAxesRepresentation'
extractBlock2Display.ScalarOpacityUnitDistance = 0.1588596196630147

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
extractBlock2Display.DataAxesGrid.XTitleFontFile = ''
extractBlock2Display.DataAxesGrid.YTitleFontFile = ''
extractBlock2Display.DataAxesGrid.ZTitleFontFile = ''
extractBlock2Display.DataAxesGrid.XLabelFontFile = ''
extractBlock2Display.DataAxesGrid.YLabelFontFile = ''
extractBlock2Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
extractBlock2Display.PolarAxes.PolarAxisTitleFontFile = ''
extractBlock2Display.PolarAxes.PolarAxisLabelFontFile = ''
extractBlock2Display.PolarAxes.LastRadialAxisTextFontFile = ''
extractBlock2Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# setup the color legend parameters for each legend in this view

# get color legend/bar for vtkBlockColorsLUT in view renderView1
vtkBlockColorsLUTColorBar = GetScalarBar(vtkBlockColorsLUT, renderView1)
vtkBlockColorsLUTColorBar.Title = 'vtkBlockColors'
vtkBlockColorsLUTColorBar.ComponentTitle = ''
vtkBlockColorsLUTColorBar.TitleFontFile = ''
vtkBlockColorsLUTColorBar.LabelFontFile = ''

# set color bar visibility
vtkBlockColorsLUTColorBar.Visibility = 1

# show color legend
extractBlock1Display.SetScalarBarVisibility(renderView1, True)

# ----------------------------------------------------------------
# setup color maps and opacity mapes used in the visualization
# note: the Get..() functions create a new object, if needed
# ----------------------------------------------------------------

# get opacity transfer function/opacity map for 'vtkBlockColors'
vtkBlockColorsPWF = GetOpacityTransferFunction('vtkBlockColors')

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(extractBlock2)
# ----------------------------------------------------------------
SaveScreenshot("mesh.png", renderView1, ImageResolution=[1280, 720])
