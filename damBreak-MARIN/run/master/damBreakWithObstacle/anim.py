# state file generated using paraview version 5.6.2

# ----------------------------------------------------------------
# setup views used in the visualization
# ----------------------------------------------------------------

# trace generated using paraview version 5.6.2
#
# To ensure correct image size when batch processing, please search 
# for and uncomment the line `# renderView*.ViewSize = [*,*]`

#### import the simple module from the paraview
from paraview.simple import *
#### disable automatic camera reset on 'Show'
paraview.simple._DisableFirstRenderCameraReset()

# get the material library
materialLibrary1 = GetMaterialLibrary()

# Create a new 'Render View'
renderView1 = CreateView('RenderView')
renderView1.ViewSize = [2152, 1276]
renderView1.AxesGrid = 'GridAxes3DActor'
renderView1.CenterOfRotation = [0.5, 0.5, 0.5]
renderView1.StereoType = 0
renderView1.CameraPosition = [1.15092032049682, 3.06895848690864, 1.29457440615615]
renderView1.CameraFocalPoint = [0.33168137947922, -0.0142890820337108, 0.285324977700519]
renderView1.CameraViewUp = [-0.0557044703320254, -0.297208520754492, 0.95318629195764]
renderView1.CameraParallelScale = 0.866025403784439
renderView1.Background = [0.32, 0.34, 0.43]
renderView1.OSPRayMaterialLibrary = materialLibrary1

# init the 'GridAxes3DActor' selected for 'AxesGrid'
renderView1.AxesGrid.XTitleFontFile = ''
renderView1.AxesGrid.YTitleFontFile = ''
renderView1.AxesGrid.ZTitleFontFile = ''
renderView1.AxesGrid.XLabelFontFile = ''
renderView1.AxesGrid.YLabelFontFile = ''
renderView1.AxesGrid.ZLabelFontFile = ''

# ----------------------------------------------------------------
# restore active view
SetActiveView(renderView1)
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# setup the data processing pipelines
# ----------------------------------------------------------------

# create a new 'OpenFOAMReader'
pvfoam = OpenFOAMReader(FileName='pv.foam')
pvfoam.SkipZeroTime = 0
pvfoam.MeshRegions = ['internalMesh', 'atmosphere', 'walls']
pvfoam.CellArrays = ['alpha.water']

# create a new 'Annotate Time Filter'
annotateTimeFilter1 = AnnotateTimeFilter(Input=pvfoam)
annotateTimeFilter1.Format = 'Time: %.2f [s]'

# create a new 'Threshold'
threshold1 = Threshold(Input=pvfoam)
threshold1.Scalars = ['CELLS', 'alpha.water']
threshold1.ThresholdRange = [0.5, 1.0]

# create a new 'Extract Block'
extractBlock1 = ExtractBlock(Input=pvfoam)
extractBlock1.BlockIndices = [4]

# ----------------------------------------------------------------
# setup the visualization in view 'renderView1'
# ----------------------------------------------------------------

# show data from threshold1
threshold1Display = Show(threshold1, renderView1)

# trace defaults for the display properties.
threshold1Display.Representation = 'Surface With Edges'
threshold1Display.ColorArrayName = [None, '']
threshold1Display.DiffuseColor = [0.0, 1.0, 1.0]
threshold1Display.OSPRayScaleArray = 'alpha.water'
threshold1Display.OSPRayScaleFunction = 'PiecewiseFunction'
threshold1Display.SelectOrientationVectors = 'None'
threshold1Display.ScaleFactor = 0.075
threshold1Display.SelectScaleArray = 'None'
threshold1Display.GlyphType = 'Arrow'
threshold1Display.GlyphTableIndexArray = 'None'
threshold1Display.GaussianRadius = 0.00375
threshold1Display.SetScaleArray = ['POINTS', 'alpha.water']
threshold1Display.ScaleTransferFunction = 'PiecewiseFunction'
threshold1Display.OpacityArray = ['POINTS', 'alpha.water']
threshold1Display.OpacityTransferFunction = 'PiecewiseFunction'
threshold1Display.DataAxesGrid = 'GridAxesRepresentation'
threshold1Display.SelectionCellLabelFontFile = ''
threshold1Display.SelectionPointLabelFontFile = ''
threshold1Display.PolarAxes = 'PolarAxesRepresentation'
threshold1Display.ScalarOpacityUnitDistance = 0.0696949125453343

# init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
threshold1Display.ScaleTransferFunction.Points = [0.125, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
threshold1Display.OpacityTransferFunction.Points = [0.125, 0.0, 0.5, 0.0, 1.0, 1.0, 0.5, 0.0]

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
threshold1Display.DataAxesGrid.XTitleFontFile = ''
threshold1Display.DataAxesGrid.YTitleFontFile = ''
threshold1Display.DataAxesGrid.ZTitleFontFile = ''
threshold1Display.DataAxesGrid.XLabelFontFile = ''
threshold1Display.DataAxesGrid.YLabelFontFile = ''
threshold1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
threshold1Display.PolarAxes.PolarAxisTitleFontFile = ''
threshold1Display.PolarAxes.PolarAxisLabelFontFile = ''
threshold1Display.PolarAxes.LastRadialAxisTextFontFile = ''
threshold1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# show data from extractBlock1
extractBlock1Display = Show(extractBlock1, renderView1)

# trace defaults for the display properties.
extractBlock1Display.Representation = 'Surface'
extractBlock1Display.ColorArrayName = ['POINTS', '']
extractBlock1Display.OSPRayScaleArray = 'alpha.water'
extractBlock1Display.OSPRayScaleFunction = 'PiecewiseFunction'
extractBlock1Display.SelectOrientationVectors = 'None'
extractBlock1Display.ScaleFactor = 0.1
extractBlock1Display.SelectScaleArray = 'None'
extractBlock1Display.GlyphType = 'Arrow'
extractBlock1Display.GlyphTableIndexArray = 'None'
extractBlock1Display.GaussianRadius = 0.005
extractBlock1Display.SetScaleArray = ['POINTS', 'alpha.water']
extractBlock1Display.ScaleTransferFunction = 'PiecewiseFunction'
extractBlock1Display.OpacityArray = ['POINTS', 'alpha.water']
extractBlock1Display.OpacityTransferFunction = 'PiecewiseFunction'
extractBlock1Display.DataAxesGrid = 'GridAxesRepresentation'
extractBlock1Display.SelectionCellLabelFontFile = ''
extractBlock1Display.SelectionPointLabelFontFile = ''
extractBlock1Display.PolarAxes = 'PolarAxesRepresentation'

# init the 'GridAxesRepresentation' selected for 'DataAxesGrid'
extractBlock1Display.DataAxesGrid.XTitleFontFile = ''
extractBlock1Display.DataAxesGrid.YTitleFontFile = ''
extractBlock1Display.DataAxesGrid.ZTitleFontFile = ''
extractBlock1Display.DataAxesGrid.XLabelFontFile = ''
extractBlock1Display.DataAxesGrid.YLabelFontFile = ''
extractBlock1Display.DataAxesGrid.ZLabelFontFile = ''

# init the 'PolarAxesRepresentation' selected for 'PolarAxes'
extractBlock1Display.PolarAxes.PolarAxisTitleFontFile = ''
extractBlock1Display.PolarAxes.PolarAxisLabelFontFile = ''
extractBlock1Display.PolarAxes.LastRadialAxisTextFontFile = ''
extractBlock1Display.PolarAxes.SecondaryRadialAxesTextFontFile = ''

# show data from annotateTimeFilter1
annotateTimeFilter1Display = Show(annotateTimeFilter1, renderView1)

# trace defaults for the display properties.
annotateTimeFilter1Display.FontFile = ''
annotateTimeFilter1Display.FontSize = 12

# ----------------------------------------------------------------
# finally, restore active source
SetActiveSource(pvfoam)
# ----------------------------------------------------------------

# save animation
AnimationScene1 = GetAnimationScene()
AnimationScene1.PlayMode = 'Snap To TimeSteps'
SaveAnimation('anim.png', renderView1, ImageResolution=[1280, 720])
