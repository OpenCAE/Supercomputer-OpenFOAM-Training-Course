set term x11 font "helvetica,17" linewidth 1.5 persist #端末,フォント,線幅,画面持続
set xrange [0.3:2] #X軸のレンジ
set style data line #データのプロットスタイルはline
plot '../../SPHERIC_Test2/case.txt' u 1:2 t "Exp."\
,'postProcessing/probes/0/p' u 1:2 t "CFD" #実験値とCFD結果(probes関数出力)のプロット
