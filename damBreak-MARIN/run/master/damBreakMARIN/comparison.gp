set terminal x11 font "helvetica,17" linewidth 1.5 persist
set xlabel "time [s]"
set ylabel "pressure [Pa]"
set xrange [0.3:2]
set style data line
caseName=system('basename $PWD')
set title caseName
plot '../../SPHERIC_Test2/case.txt' u 1:2 t "Exp." with l lw 2\
, '../damBreakMARIN/postProcessing/probes/0/p' u 1:2 t "damBreakMARIN"\
, 'postProcessing/probes/0/p' u 1:2 t caseName
