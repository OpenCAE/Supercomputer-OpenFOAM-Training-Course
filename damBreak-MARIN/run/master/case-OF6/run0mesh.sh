#!/bin/sh
#PJM -g gt00
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=56
#PJM -L elapse=0:15:00
#PJM -S
module purge                                   # ここからOpenFOAMの環境設定
module load gcc
module load openfoam/6
source $WM_PROJECT_DIR/etc/bashrc              # ここまでOpenFOAMの環境設定 
blockMesh >& log.blockMesh                     # ベース格子生成
snappyHexMesh -overwrite >& log.snappyHexMesh  # 格子と水柱のセルゾーン生成
checkMesh >& log.checkMesh                     # 格子品質の検査
