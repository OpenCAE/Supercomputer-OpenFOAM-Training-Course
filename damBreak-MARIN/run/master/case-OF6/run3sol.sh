#!/bin/sh
#PJM -g gt00
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=32
#PJM -L elapse=0:15:00
#PJM -S
module purge                                   # ここからOpenFOAMの環境設定
module load gcc
module load openfoam/6
source $WM_PROJECT_DIR/etc/bashrc              # ここまでOpenFOAMの環境設定 
# MPI並列実行
# OpenFOAMのユーティリティやソルバーのMPI並列計算時にはparallelオプションが必要
# -n MPIプロセス数
# 環境変数PJM_MPI_PROC: --mpi proc=Nで指定したMPIプロセス数
# 環境変数PJM_JOBID: ジョブID．ソルバ再実行時にログが上書きされないようにする．
mpiexec.hydra -n ${PJM_MPI_PROC} interFoam -parallel >& log.interFoam.${PJM_JOBID}
