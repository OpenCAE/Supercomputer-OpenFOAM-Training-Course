#!/bin/bash
module purge                      # 標準で有効なmoduleをpurgeで全てunload
module load gcc/4.8.5             # Gcc-4.8.5のmoduleをload
module load openfoam/v1812        # OpenFOAM-v1812のmoduleをload
source $WM_PROJECT_DIR/etc/bashrc # OpenFOAMの環境設定
jobid=${PJM_SUBJOBID:-$PJM_JOBID} # サブジョブIDまたジョブID
