#!/bin/bash
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runshare.sh              # 共通設定
module load vtune                 # VTuneのmoduleをload
# ソルバ実行のプロファイリング
# amplxe-cl : CUI版のVTuneコマンド
# -c hotspots : ホットスポットの情報を収集
# -r 保存ディレクトリ:収集MPIランク範囲
amplxe-cl -c hotspots -r vtune.$jobid \
icoFoam >& log.icoFoam.$jobid
