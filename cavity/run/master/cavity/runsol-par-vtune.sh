#!/bin/bash
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runshare.sh              # 共通設定
module load vtune                 # VTuneのmoduleをload
export I_MPI_DEBUG=5              # Intel MPIのDEBUG情報レベル
# 並列ソルバ実行のプロファイリング
# amplxe-cl : CUI版のVTuneコマンド
# -c hotspots : ホットスポットの情報を収集
# -r 保存ディレクトリ:収集MPIランク範囲
mpiexec.hydra -n $PJM_MPI_PROC \
-gtool "amplxe-cl -c hotspots -r vtune.$jobid:all=node-wide" \
icoFoam -parallel >& log.icoFoam-par.$jobid
