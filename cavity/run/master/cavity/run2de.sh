#!/bin/bash
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runshare.sh              # 共通設定
# 領域分割
# -cellDist : 格子の領域番号を場cellDistに出力(可視化用．必須ではない)
decomposePar -cellDist >& log.decomposePar.$jobid
