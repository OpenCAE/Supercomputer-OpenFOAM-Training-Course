#!/bin/bash
#PJM -L rscgrp=lecture
#PJM -L node=1
#PJM --mpi proc=4
#PJM -L elapse=0:15:00
#PJM -g gt00
#PJM -S
source ./runshare.sh              # 共通設定
export I_MPI_DEBUG=5              # Intel MPIのDEBUG情報レベル
# 並列ソルバ実行
# mpiexec.hydra    : Intel MPIのMPI並列実行コマンド
# -n $PJM_MPI_PROC : プロセス数指定(PJMによりPJM_MPI_PROCに設定される)
# -parallel        : 並列実行の指定(OpenFOAMのコマンド共通のオプション)
mpiexec.hydra -n $PJM_MPI_PROC \
icoFoam -parallel >& log.icoFoam-par.$jobid
